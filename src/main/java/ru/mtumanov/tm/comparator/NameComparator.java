package ru.mtumanov.tm.comparator;

import java.util.Comparator;

import ru.mtumanov.tm.api.model.IHaveName;

public enum NameComparator implements Comparator<IHaveName> {

    INSTANCE;

    @Override
    public int compare(final IHaveName o1, final IHaveName o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getName() == null || o2.getName() == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
