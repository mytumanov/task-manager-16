package ru.mtumanov.tm.api.controller;

import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;

public interface IProjectController {

    void createProject() throws AbstractFieldException, AbstractEntityNotFoundException;

    void showProjectById() throws AbstractFieldException;

    void showProjectByIndex() throws AbstractFieldException;

    void showProjects();

    void clearProjects();

    void removeProjectById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void removeProjectByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void updateProjectById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void updateProjectByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void startProjectByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void startProjectById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void completeProjectByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void completeProjectById() throws AbstractFieldException, AbstractEntityNotFoundException;

    void changeProjectStatusByIndex() throws AbstractFieldException, AbstractEntityNotFoundException;

    void changeProjectStatusById() throws AbstractFieldException, AbstractEntityNotFoundException;

}
