package ru.mtumanov.tm.api.service;

import java.util.Comparator;
import java.util.List;

import ru.mtumanov.tm.enumerated.Sort;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.exception.field.AbstractFieldException;
import ru.mtumanov.tm.model.Task;

public interface ITaskService {

    void add(Task task) throws AbstractEntityNotFoundException;

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAll(Sort sort);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id) throws AbstractFieldException;

    Task findOneByIndex(Integer index) throws AbstractFieldException;

    void remove(Task task) throws AbstractEntityNotFoundException;

    Task removeById(String id) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task removeByIndex(Integer index) throws AbstractFieldException, AbstractEntityNotFoundException;

    void clear();

    Task create(String name, String description) throws AbstractFieldException;

    Task updateById(String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task changeTaskStatusById(String id, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

}
