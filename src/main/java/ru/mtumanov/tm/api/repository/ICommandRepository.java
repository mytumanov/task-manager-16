package ru.mtumanov.tm.api.repository;

import ru.mtumanov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
